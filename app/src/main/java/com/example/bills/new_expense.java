package com.example.bills;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class new_expense extends AppCompatActivity {

    private EditText date;
    private EditText store;
    private EditText spent;
    private Button button;

    DatabaseHelperTest db;
    // The class for adding new expense
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_expense);

        db = new DatabaseHelperTest(this);

        date = (EditText)findViewById(R.id.newExpense_date);
        store = (EditText)findViewById(R.id.newExpense_store);
        spent = (EditText)findViewById(R.id.newExpense_spent);

        button = (Button)findViewById(R.id.newExpense_button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String date1 = date.getEditableText().toString();
                String store1 = store.getEditableText().toString();
                String spent1 = spent.getEditableText().toString();
                // Checking that there are no empty fields
                if(date1.equals("")||store1.equals("")||spent1.equals("")){

                    Toast.makeText(getApplicationContext(),"Incorect entry. Try Again.",Toast.LENGTH_SHORT).show();

                }else {

                    // Getting the data passed from the previous screen and saving it int the approriapte data types

                    Intent intent = getIntent();
                    String user_email = intent.getStringExtra("user_email");
                    String category_name = intent.getStringExtra("category_name");

                    //Inserting the new entry into the database

                    Boolean insert = db.insertExpense(store1, Integer.parseInt(spent1), user_email, category_name);
                    Toast.makeText(getApplicationContext(), store1 + " " + spent1 + " " + user_email, Toast.LENGTH_SHORT).show();
                    if (insert == true) {

                        Toast.makeText(getApplicationContext(), "Expense added", Toast.LENGTH_SHORT).show();

                    } else {

                        Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_SHORT).show();
                    }

                    //Transfering to the appropriate screen(In this case the screen where the expenses of the different categories are displayed
                    intent = new Intent(new_expense.this, expenses.class);
                    intent.putExtra("user_email", user_email);
                    intent.putExtra("category_name", category_name);
                    startActivity(intent);

                }



            }
        });
    }
}
