package com.example.bills;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

// Database helper class responsible for the creation of the different tables and the functions for inserting and retreiving data from the database
public class DatabaseHelperTest extends SQLiteOpenHelper {


    public DatabaseHelperTest(@Nullable Context context) {
        super(context, "bills.db", null, 1);
    }

    //Creating the tables in the database with the relations between them
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("PRAGMA foreign_keys=ON");

        db.execSQL("Create table users(email text primary key, password text)");
        db.execSQL("Create table categories(name TEXT primary key , planned INTEGER, spent INTEGER, remain INTEGER, user_email TEXT, FOREIGN KEY(user_email) REFERENCES users(email))");
        db.execSQL("Create table expenses(id INTEGER primary key AUTOINCREMENT, name TEXT, amount INTEGER, user_email TEXT, category_name TEXT, FOREIGN KEY(user_email) REFERENCES users(email), FOREIGN KEY(category_name) REFERENCES categories(name))");

    }

    private static void setForeignKeyConstraintsEnabled(@NonNull SQLiteDatabase db) {
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=1;");
        }
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        db.execSQL("drop table if exists users");
    }

    //Iserting new user into the database
    public boolean insertUser(String email, String password){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("email",email);
        contentValues.put("password", password);

        long ins = db.insert("users", "null", contentValues);
        if(ins == -1){

            return false;

        }else{

            return true;
        }
    }

    //Iserting a category to the categories table of a certain user
    public boolean insertCategory(String name, int planned, int spent, int remain, String user_email){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("planned", planned);
        contentValues.put("spent", spent);
        contentValues.put("remain", remain);
        contentValues.put("user_email", user_email);

        long ins = db.insert("categories", "null", contentValues);
        if(ins == -1){
            return false;
        }else{
            return true;
        }
    }

    //Iserting an expense into the exepnses table of a certain category of a ceratin user
    public boolean insertExpense(String name, int amount, String user_email, String category_name){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("amount", amount);
        contentValues.put("user_email", user_email);
        contentValues.put("category_name", category_name);

        long ins = db.insert("expenses", "null", contentValues);

        if(ins == -1){
            return false;
        }else{
            return true;
        }
    }

    //Checking if an email has already been registered
    public boolean checkmail(String email){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from users where email=?", new String[]{email});
        if(cursor.getCount()>0){

            return false;

        }else{

            return true;

        }
    }

    //Checking for a record in the database with certain email and password
    public Boolean emailPasword(String email, String password){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from users where email=? and password=?",new String[]{email,password});
        if(cursor.getCount() > 0){

            db.close();
            return true;
        }
        else{
            db.close();
            return false;
        }
    }
    //Getting all the categories added by a certaing user
    public Cursor getCategories(String user_email){

        String query = "SELECT * FROM categories WHERE user_email=?";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from categories where user_email=?", new String[]{user_email});

        return cursor;
    }

    //Getting all the expenses added to certaing category by a certain user
    public Cursor getExpenses(String user_email, String category){

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from expenses where user_email=? and category_name=?", new String[]{user_email, category});

        return cursor;
    }
}