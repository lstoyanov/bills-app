package com.example.bills;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class Login extends AppCompatActivity {

    private TextView registerHere;
    private Button loginBtn;
    private EditText email_login, password_login;
    DatabaseHelperTest db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        db = new DatabaseHelperTest(this);

        email_login = (EditText)findViewById(R.id.email_login);
        password_login = (EditText)findViewById(R.id.passowrd_login);

        registerHere = (TextView) findViewById(R.id.register_login);
        loginBtn = (Button) findViewById(R.id.login);

        registerHere.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                openRegisterActivity();

            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                // Getting the email and the password entered and checking if they match to the records in the database
                String email_login1 = email_login.getText().toString();
                String password_login1 = password_login.getText().toString();

                Boolean checkEmailPass = db.emailPasword(email_login1, password_login1);
                if(checkEmailPass == true){

                    openMainScreenActivity(email_login1);
                }else{

                    Toast.makeText(getApplicationContext(),"Ivalid credentials!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    //The function transitioning the user to the main screen(In this case the Categories screen)
    private void openMainScreenActivity(String user_email){

//        Toast.makeText(getApplicationContext(),user_email,Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, planned.class);
        intent.putExtra("user_email", user_email);
        startActivity(intent);
    }
    //The function transitioning the user to the register screen
    private void openRegisterActivity(){

        Intent intent = new Intent(this, Register.class);
        startActivity(intent);

    }
}
