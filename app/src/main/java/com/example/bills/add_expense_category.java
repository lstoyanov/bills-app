package com.example.bills;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class add_expense_category extends AppCompatActivity {

    private TextView category;
    private TextView planned;
    private TextView spent;
    private TextView remain;
    private Button button;

    DatabaseHelperTest db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expense_category);

        db = new DatabaseHelperTest(this);

        category = (EditText)findViewById(R.id.addExpenseCategory);
        planned = (EditText)findViewById(R.id.addPlannedToSpend);
        spent = (EditText)findViewById(R.id.addSpent);
        remain = (EditText)findViewById(R.id.addRemain);

        button = (Button)findViewById(R.id.addExpense);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String category1 = category.getEditableText().toString();
                String planned1 = planned.getEditableText().toString();
                String spent1 = spent.getEditableText().toString();
                String remain1 = remain.getEditableText().toString();

                //Checking for any empty fields
                if(category1.equals("")||spent1.equals("")||planned1.equals("")||remain1.equals("")){

                    Toast.makeText(getApplicationContext(),"Incorect entry. Try Again.",Toast.LENGTH_SHORT).show();

                }else {

                    //Getting the data passed from the previous scree and inserting the new category entry into the database
                    Intent intent = getIntent();
                    String user_email = intent.getStringExtra("user_email");
                    Boolean insert = db.insertCategory(category1, Integer.parseInt(planned1), Integer.parseInt(spent1),Integer.parseInt(remain1), user_email);
//                    Toast.makeText(getApplicationContext(), user_email, Toast.LENGTH_SHORT).show();
                    if(insert == true){

//                        Toast.makeText(getApplicationContext(), "Category added", Toast.LENGTH_SHORT).show();
                    }

                    //Transfering to the screen with the categories
                    intent = new Intent(add_expense_category.this, planned.class);
                    intent.putExtra("user_email", user_email);
                    startActivity(intent);
                }
            }
        });
    }
}
