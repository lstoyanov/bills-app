package com.example.bills;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class planned extends Activity {
    // Initializing a database variable for accessing the database along with the text fields and layouts used in the design
    DatabaseHelperTest db;
    private TextView addItem;
    private TextView addItem2;
    private int count = 1;
    private LinearLayout works_items;
    private LinearLayout expenses_item;
    private TextView headCategory;
    private ArrayList categoryNames, categoryPlanned, categorySpent, categoryRemain;
    private String user_email;
    private static final String TAG = planned.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        final Random rand = new Random();

        db = new DatabaseHelperTest(this);

        setContentView(R.layout.activity_planned);
        //Getting the passed from the previous menu data(in this case the user's unique email)
        Intent intent = getIntent();
        user_email = intent.getStringExtra("user_email");
//        Toast.makeText(getApplicationContext(),user_email,Toast.LENGTH_SHORT).show();

        categoryNames = new ArrayList<>();
        categoryPlanned = new ArrayList<>();
        categorySpent = new ArrayList<>();
        categoryRemain = new ArrayList<>();

        addItem = (TextView) findViewById(R.id.textView5);
        addItem2 = (TextView) findViewById(R.id.textView6);
        works_items = (LinearLayout)findViewById(R.id.work_items);

        headCategory = (TextView)findViewById(R.id.expense_categories_category);
        // Setting on click listener functions for the buttons in the activity
        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(planned.this, add_work.class);

                startActivityForResult(intent, 1);

            }
        });

        addItem2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Intent intent = new Intent(planned.this, add_expense_category.class);
                intent.putExtra("user_email", user_email);
                startActivity(intent);

            }
        });

        storeDataIntoArrays(user_email);
//        Toast.makeText(getApplicationContext(), user_email, Toast.LENGTH_SHORT).show();
        displayData();

    }
    // Stiring the needed to display data for this activity into arrays for future use
    protected void storeDataIntoArrays(String user_email){
//        Toast.makeText(getApplicationContext(),"invoked", Toast.LENGTH_SHORT).show();
        Cursor cursor = db.getCategories(user_email);
        if(cursor.getCount() == 0){

            Toast.makeText(getApplicationContext(), "No data", Toast.LENGTH_SHORT);

        }else{
//            Toast.makeText(getApplicationContext(),"Have data", Toast.LENGTH_SHORT).show();
            while(cursor.moveToNext()){

                categoryNames.add(cursor.getString(0));
                categoryPlanned.add(cursor.getInt(1));
                categorySpent.add(cursor.getInt(2));
                categoryRemain.add(cursor.getInt(3));
            }
        }
    }
    // Displaying the needed data from the database
    protected void displayData(){


//        Toast.makeText(getApplicationContext(), "shoot", Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(), Integer.toString(categoryNames.size()), Toast.LENGTH_SHORT).show();
        for(int i = 0; i < categoryNames.size(); i++){

            final View childView1;
            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            childView1 = inflater.inflate(R.layout.item_category, null);

            final int index = count;
            final TextView categoryView = (TextView)childView1.findViewById(R.id.expense_category);
            final TextView plannedView = (TextView)childView1.findViewById(R.id.expense_planned);
            final TextView spentView = (TextView)childView1.findViewById(R.id.expense_spent);
            final TextView remainView = (TextView)childView1.findViewById(R.id.expense_remain);

            childView1.setId(index);

            categoryView.setText((String) categoryNames.get(i));
            plannedView.setText(Integer.toString((Integer)categoryPlanned.get(i)));
            spentView.setText(Integer.toString((Integer)categorySpent.get(i)));
            remainView.setText(Integer.toString((Integer)categoryRemain.get(i)));

            count += 1;

            expenses_item = (LinearLayout)findViewById(R.id.expenses_items);
            expenses_item.addView(childView1);

            final LinearLayout row = (LinearLayout)childView1.findViewById(R.id.expense);

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Toast.makeText(getApplicationContext(), "click", Toast.LENGTH_SHORT).show();

                    openCategory((String)categoryView.getText(), user_email);

                }
            });

        }

    }
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data)
//    {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if(resultCode == RESULT_OK){
//            if(requestCode == 1 && data !=null) {
//
//                String work = (String)data.getStringExtra("work");
//                String income = (String)data.getStringExtra("income");
//
//                final View childView;
//                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                childView = inflater.inflate(R.layout.item_work, null);
//
//                final int index = count;
//
//                childView.setId(index);
//
//                final TextView workView = (TextView)childView.findViewById(R.id.work_work);
//                workView.setText(work);
//
//                final TextView incomeView = (TextView)childView.findViewById(R.id.work_income);
//                incomeView.setText(income);
//
//                count += 1;
//
//                expenses_item = (LinearLayout)findViewById(R.id.work_items);
//                expenses_item.addView(childView);
//
//                LinearLayout row = (LinearLayout)childView.findViewById(R.id.work);
//
//                row.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        Toast toast=Toast. makeText(getApplicationContext(), workView.getText(),Toast. LENGTH_SHORT);
//                        toast.show();
//
//                    }
//                });
//
//            }
//
//            if(requestCode == 2 && data !=null) {
//
////                String category = (String)data.getStringExtra("category");
////                String planned = (String)data.getStringExtra("planned");
////                String spent = (String)data.getStringExtra("spent");
////                String remain = (String)data.getStringExtra("remain");
////
////                final View childView1;
////                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
////                childView1 = inflater.inflate(R.layout.item_category, null);
////
////                final int index = count;
////
////                childView1.setId(index);
////
////                final TextView categoryView = (TextView)childView1.findViewById(R.id.expense_category);
////                categoryView.setText(category);
////
////                final TextView plannedView = (TextView)childView1.findViewById(R.id.expense_planned);
////                plannedView.setText(planned);
////
////                final TextView spentView = (TextView)childView1.findViewById(R.id.expense_spent);
////                spentView.setText(spent);
////
////                final TextView remainView = (TextView)childView1.findViewById(R.id.expense_remain);
////                remainView.setText(remain);
////
////                count += 1;
////                categoryNames.add((String) categoryView.getText());
////                expenses_item = (LinearLayout)findViewById(R.id.expenses_items);
////                expenses_item.addView(childView1);
////
////                LinearLayout row = (LinearLayout)childView1.findViewById(R.id.expense);
////
////                row.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View view) {
////
////                        openCategory((String)categoryView.getText(), user_email);
////
////                    }
////                });
////
//            }
//
//        }
//
//    }
    //Function resposinble for transitioning between activities
    private void openCategory(String category_name, String user_email){

        Intent intent = new Intent(this, expenses.class);
        intent.putExtra("category_name", category_name);
        intent.putExtra("user_email", user_email);
        startActivity(intent);


    }

}

