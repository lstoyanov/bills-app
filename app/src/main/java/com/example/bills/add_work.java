package com.example.bills;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class add_work extends AppCompatActivity {

    private TextView work;
    private TextView income;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_work);

        work = (EditText)findViewById(R.id.addWork_work);
        income = (EditText)findViewById(R.id.addWork_income);

        button = (Button)findViewById(R.id.addWork_button);

        //Setting onClick listener for the button in this screen
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.putExtra("work", work.getEditableText().toString());
                intent.putExtra("income", income.getEditableText().toString());

                setResult(RESULT_OK, intent);
                finish();

            }
        });
    }
}
