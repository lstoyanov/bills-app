package com.example.bills;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class expenses extends AppCompatActivity  {

    DatabaseHelperTest db;
    private Spinner spinner;
    private TextView header;
    private Button addExpense, goBack;
    private int count = 0;
    private LinearLayout row_expense;
    private Hashtable<String, ArrayList> categories;
    private ArrayList<ArrayList> expenses;
    private ArrayList expensesDates, expensesNames, expensesSpent;

    private String user_email;
    private String category_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses);
        db = new DatabaseHelperTest(this);

        // Creating new arrays to hold the data from the database needed to be displayed for this screen
        expensesNames = new ArrayList<>();
        expensesSpent = new ArrayList<>();

        //Getting the data passed fro the previous scree(activity).In this case the category to which the expenses belong and the email of the user currently logged in
        Intent intent = getIntent();
        category_name = intent.getStringExtra("category_name");
        user_email = intent.getStringExtra("user_email");

        categories = new Hashtable<String, ArrayList>();
        spinner = (Spinner) findViewById(R.id.spinner);
        header = (TextView)findViewById(R.id.expense_categories_category);
        header.setText(category_name);

        goBack = (Button)findViewById(R.id.back_button);
        addExpense = (Button)findViewById(R.id.expense_categories_button);

        //Setting onClick listener functions for the buttons in this screen

        addExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(expenses.this, new_expense.class);
                intent.putExtra("user_email", user_email);
                intent.putExtra("category_name", category_name);
                startActivity(intent);

            }
        });

        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(expenses.this, planned.class);
                intent.putExtra("user_email", user_email);
                startActivity((intent));

            }
        });

        // Creating a drop down menu with all currently added categories from the user

        List<String> categoriesSpiner = new ArrayList<>();

        categoriesSpiner.add(0, "Choose category");
        if(categories.isEmpty()){

            categories.put(category_name, new ArrayList());
            categoriesSpiner.add(category_name);
        }
        else if(!categories.containsKey(category_name)){

            categories.put(category_name, new ArrayList());
            categoriesSpiner.add(category_name);

        }else{
            for(String key: categories.keySet()){

                categoriesSpiner.add(key);
            }
        }

        ArrayAdapter<String>dataAdapter;
        dataAdapter = new ArrayAdapter(this,R.layout.support_simple_spinner_dropdown_item, categoriesSpiner);


        // Function for selecting a category from the drop down menu

        dataAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (adapterView.getItemAtPosition(i).equals("Choose category")) {

                } else {

                    String item = adapterView.getItemAtPosition(i).toString();
                    header.setText(item);
                    Toast.makeText(adapterView.getContext(), "Selected"+item, Toast.LENGTH_SHORT).show();

                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        int selectionPosition= dataAdapter.getPosition(category_name);
        spinner.setSelection(selectionPosition);

        storeDataIntoArrays(user_email, category_name);
        displayData();
    }

    // Storing the needed for this screen data into arrays for future use
    protected void storeDataIntoArrays(String user_email, String category_name){

        Cursor cursor = db.getExpenses(user_email, category_name);
        if(cursor.getCount() == 0){

            Toast.makeText(getApplicationContext(), "No data", Toast.LENGTH_SHORT);

        }else{

            while(cursor.moveToNext()){

                expensesNames.add(cursor.getString(1));
                expensesSpent.add(cursor.getInt(2));
            }
        }
    }
    // Function to display all the needed data for this screen
    protected void displayData(){

        Toast.makeText(getApplicationContext(), Integer.toString(expensesNames.size()), Toast.LENGTH_SHORT).show();
        for(int i = 0; i < expensesNames.size(); i++){

            final View childView1;
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            childView1 = inflater.inflate(R.layout.item_expense, null);

            final int index = count;

            final TextView storeView = (TextView) childView1.findViewById(R.id.item_expense_store);
            final TextView spentView = (TextView) childView1.findViewById(R.id.item_expense_spent);

            childView1.setId(index);

            storeView.setText((String)expensesNames.get(i));
            spentView.setText(Integer.toString((Integer)expensesSpent.get(i)));

            count += 1;

            row_expense = (LinearLayout) findViewById(R.id.items_expenses);
            row_expense.addView(childView1);

            final LinearLayout row = (LinearLayout) childView1.findViewById(R.id.item_expense);

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Toast.makeText(getApplicationContext(), "click", Toast.LENGTH_SHORT).show();

                }
            });

        }

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (resultCode == RESULT_OK) {
//            if (requestCode == 1 && data != null) {
//
//                String date = (String) data.getStringExtra("date");
//                String store = (String) data.getStringExtra("store");
//                String spent = (String) data.getStringExtra("spent");
//
//                final View childView;
//                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                childView = inflater.inflate(R.layout.item_expense, null);
//
//                final int index = count;
//
//                childView.setId(index);
//
//                final TextView dateView = (TextView) childView.findViewById(R.id.item_expense_date);
//                dateView.setText(date);
//
//                final TextView storeView = (TextView) childView.findViewById(R.id.item_expense_store);
//                storeView.setText(store);
//
//                final TextView spentView = (TextView) childView.findViewById(R.id.item_expense_spent);
//                spentView.setText(spent);
//
//                count += 1;
//
//                row_expense = (LinearLayout) findViewById(R.id.items_expenses);
//                row_expense.addView(childView);
//
//                LinearLayout row = (LinearLayout) childView.findViewById(R.id.item_expense);
//
//                row.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        Toast toast = Toast.makeText(getApplicationContext(), storeView.getText(), Toast.LENGTH_SHORT);
//                        toast.show();
//
//                    }
//                });
//
//            }
//
//        }
//    }


}
