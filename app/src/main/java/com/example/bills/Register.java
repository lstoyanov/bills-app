package com.example.bills;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends AppCompatActivity {

    DatabaseHelperTest db; // Initializing a database
    EditText email, password_register, repeat_password_register; //Initializing edithText fields corresponding to the edithText fields in the activity(the menu design page)
    private Button registerBtn; // Initializing button for submiting registration form.
    private Button cancelBtn; // Initializing a button for cancilng a new regitration.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DatabaseHelperTest(this); // Creating a new database
        setContentView(R.layout.activity_register); // Setting the activity register

        // Assigning the edithText fields from the register activity to the initialy initialized variables EdithText by using the id set when creating the design.
        email = (EditText)findViewById(R.id.email_register);
        password_register = (EditText)findViewById(R.id.passowrd_register);
        repeat_password_register = (EditText)findViewById((R.id.repeat_passowrd_register));

        //Assigning the buttons using their id
        registerBtn = (Button)findViewById(R.id.register);
        cancelBtn = (Button) findViewById(R.id.cancel);

        //Creating onClick function for submitting a registration form by clicking the register button
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Getting the data from the edithText fields and saving it it in the correct data type
                String email1 = email.getText().toString();
                String password_register1 = password_register.getText().toString();
                String repeat_password_register1 = repeat_password_register.getText().toString();

                // Checking if there are are empty fields
                if(email1.equals("")||password_register1.equals("")||repeat_password_register1.equals("")){
                    Toast.makeText(getApplicationContext(),"Incorect entry. Try Again.",Toast.LENGTH_SHORT).show();

                }else{
                    //Cheching that the password and the repeated password match
                    Boolean check = password_register1.equals(repeat_password_register1);
                    if(check== true){
                        Toast.makeText(getApplicationContext(),"True", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(),"False", Toast.LENGTH_SHORT).show();
                    }

                    if(password_register1.equals(repeat_password_register1)){

                        //Checking in the database that there is no other user using the same email address
                        Boolean checkmail = db.checkmail(email1);

                        if(checkmail == true){
                            // Inserting the newly created account into the database with the chosen by the user email and password
                            Boolean insert = db.insertUser(email1, password_register1);
                            if(insert == true){

                                Toast.makeText(getApplicationContext(), "Registered successfully", Toast.LENGTH_SHORT).show();
                            }
                            openLoginActivity();
                        }
                        else{

                            Toast.makeText(getApplicationContext(), "Email already exists", Toast.LENGTH_SHORT).show();
                        }


                    }else{

                        Toast.makeText(getApplicationContext(),"Passwords don't match", Toast.LENGTH_SHORT).show();
                    }


                }

            }
        });
        // Setting an onClick listener function for the cancel button to take the user back to the Login screen
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openLoginActivity();
            }
        });
    }
    // The function which is responsible for transitioning to the Login screen.
    private void openLoginActivity(){

        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }
}
